user = {{ vault_db_usermail_user }}
password = {{ vault_db_usermail_password }}
hosts = 127.0.0.1
dbname = {{ postfix_postgres_db }}
query = SELECT destination FROM virtual_aliases WHERE source='%s'