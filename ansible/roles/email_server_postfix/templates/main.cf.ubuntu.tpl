smtpd_banner = $myhostname ESMTP $mail_name (Ubuntu)
biff = no

append_dot_mydomain = no


readme_directory = no

compatibility_level = 2



smtpd_tls_cert_file={{ letsencrypt_certs_directory }}/{{ inventory_hostname }}.crt
smtpd_tls_key_file={{ letsencrypt_keys_directory }}/{{ inventory_hostname }}.key
smtpd_tls_security_level=encrypt
smtpd_use_tls=yes
smtp_use_tls=yes
smtp_tls_security_level=encrypt
smtp_tls_CApath=/etc/ssl/certs
smtp_tls_session_cache_database = btree:${data_directory}/smtp_scache


smtpd_relay_restrictions = permit_mynetworks permit_sasl_authenticated defer_unauth_destination
myhostname = {{ inventory_hostname }}
alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases
myorigin = /etc/mailname
mydestination = localhost
relayhost =
mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128
mailbox_size_limit = 0
recipient_delimiter = +
inet_interfaces = all
inet_protocols = all

smtpd_sasl_type = dovecot
smtpd_sasl_path = private/auth
smtpd_sasl_auth_enable = yes
smtpd_recipient_restrictions =permit_sasl_authenticated,permit_mynetworks,reject_unauth_destination

virtual_transport = lmtp:unix:private/dovecot-lmtp
virtual_mailbox_domains = pgsql:/etc/postfix/pgsql-virtual-mailbox-domains.cf
virtual_mailbox_maps = pgsql:/etc/postfix/pgsql-virtual-mailbox-maps.cf
virtual_alias_maps = pgsql:/etc/postfix/pgsql-virtual-alias-maps.cf