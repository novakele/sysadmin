driver = pgsql
connect = host=127.0.0.1 dbname={{ postfix_postgres_db }} user={{ vault_db_usermail_user }} password={{ vault_db_usermail_password }}
default_pass_scheme = SHA512-CRYPT
password_query = SELECT email as user, password FROM virtual_users WHERE email='%u';
