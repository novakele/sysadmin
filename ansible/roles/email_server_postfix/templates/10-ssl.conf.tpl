ssl = required

ssl_cert = <{{ letsencrypt_certs_directory }}/{{ inventory_hostname }}.crt
ssl_key = <{{ letsencrypt_keys_directory }}/{{ inventory_hostname }}.key

ssl_cipher_list = PROFILE=SYSTEM