mail_location = maildir:/var/mail/vhosts/%d/%n

namespace inbox {
  inbox = yes
}

mail_privileged_group = mail

first_valid_uid = 1000

protocol !indexer-worker {
}

mbox_write_locks = fcntl