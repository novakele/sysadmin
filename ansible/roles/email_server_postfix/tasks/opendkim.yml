- name: Create directory structure from opendkim
  file:
    path: "/etc/opendkim/keys/{{ domain_name}}"
    mode: o=rwX,g=,o=
    state: directory
    owner: opendkim
    group: opendkim
    recurse: yes

- name: Push opendkim configuration file
  template:
    src: opendkim.conf.tpl
    dest: /etc/opendkim.conf
    owner: root
    group: root
    mode: "0644"

- name: Add configuration to SigningTable file
  blockinfile:
    path: /etc/opendkim/SigningTable
    create: yes
    block: |
      *@{{ domain_name }} {{ dkim_name }}.{{ domain_name }}

- name: Add configuration to KeyTable file
  blockinfile:
    path: /etc/opendkim/KeyTable
    create: yes
    block: |
      {{ dkim_name }}.{{ domain_name }} {{ domain_name }}:{{ dkim_id }}:/etc/opendkim/keys/{{ domain_name }}/{{ dkim_id }}.private

- name: Add configuration to TrustedHosts
  blockinfile:
    path: /etc/opendkim/TrustedHosts
    create: yes
    block: |
      127.0.0.1
      ::1
      *.{{ domain_name }}

- name: Check if we already generated DKIM key pair
  stat:
    path: /etc/opendkim/keys/{{ domain_name }}/{{ dkim_id }}.private
  register: dkim_keypair_state

- name: Generate DKIM key pair
  command: "opendkim-genkey -b 2048 -d  {{ domain_name }} -D /etc/opendkim/keys/{{ domain_name }} -s {{ dkim_id }} -v"
  when: not dkim_keypair_state.stat.exists

# Quick and dirty way to get the record.
# The record is actually split on two lines with a mix of \n, \n\t and \t
# I tried using the slurp module and applying a few filters to get the same output, but it was way worse
# At least now, it's somewhat readable.
- name: Parse file containing DKIM TXT record
  shell: "head /etc/opendkim/keys/{{ domain_name }}/{{ dkim_id }}.txt | tr -d '[:space:]' | cut -d'(' -f2 | cut -d')' -f1 | tr -d '\"'"
  register: dkim_record_parsed

- name: Define DKIM TXT record
  set_fact:
    dkim_record: "{{ dkim_record_parsed.stdout }}"

- name: Restart OpenDKIM service
  systemd:
    name: opendkim
    state: restarted
    enabled: yes
