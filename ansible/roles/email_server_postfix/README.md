Email Server Postfix
=========

Fully Configure an email server to a DigitalOcean droplet

Requirements
------------

Deploy the droplet and the initial DNS configuration using the Terraform plan.

Role Variables
--------------

TODO

Dependencies
------------

No other dependencies

Example Playbook
----------------

Please refer to `deploy_email_servers` playbook.

License
-------

MIT
