# Error: Could not determine network mappings from files in path
# Solution: https://www.vgemba.net/vmware/Packer-Workstation-Error/
packer {
  required_plugins {
    vmware = {
      version = ">= 1.0.3"
      source  = "github.com/hashicorp/vmware"
    }
  }
}

source "vmware-iso" "windows-7" {
  iso_url           = "http://care.dlservice.microsoft.com/dl/download/evalx/win7/x64/EN/7600.16385.090713-1255_x64fre_enterprise_en-us_EVAL_Eval_Enterprise-GRMCENXEVAL_EN_DVD.iso"
  iso_checksum      = "md5:1d0d239a252cb53e466d39e752b17c28"
  communicator      = "winrm"
  guest_os_type     = "windows7-64"
  winrm_username    = "vagrant"
  winrm_password    = "vagrant"
  winrm_timeout     = "45m"
  shutdown_command  = "shutdown /s /t 10 /f /d p:4:1 /c \"Packer Shutdown\""
  cpus              = 4
  memory            = 4096 # 4GB
  cores             = 2
  disk_size         = 80000 # 80GB
  disk_adapter_type = "lsisas1068"
  sound             = false
  # https://www.windowscentral.com/how-create-unattended-media-do-automated-installation-windows-10
  floppy_files = [
    "./answers/Autounattend.xml",
    # https://github.com/StefanScherer/packer-windows/blob/main/scripts/enable-winrm.ps1
    "../scripts/enable_winrm.ps1",
  ]
  floppy_label = "floppy"
}

build {
  name = "windows 7"
  sources = [
    "source.vmware-iso.windows-7"
  ]

  # https://github.com/StefanScherer/packer-windows/blob/main/scripts/win-7-update-sp1.ps1
  provisioner "powershell" {
    script = "../scripts/win-7-update-sp1.ps1"
  }

  provisioner "windows-restart" {
    restart_timeout = "20m"
  }

  # https://github.com/StefanScherer/packer-windows/blob/main/scripts/win-7-update-net48.ps1
  provisioner "powershell" {
    script = "../scripts/win-7-update-net48.ps1"
  }

  provisioner "windows-restart" {
    restart_timeout = "20m"
  }

  # https://github.com/StefanScherer/packer-windows/blob/main/scripts/win-7-update-powershell-5.1.ps1
  # provisioner "powershell" {
  #   script = "../scripts/win-7-update-powershell-5.1.ps1"
  # }

  provisioner "windows-restart" {
    restart_timeout = "20m"
  }

  # # https://github.com/StefanScherer/packer-windows/blob/main/scripts/chocolatey.bat
  # provisioner "windows-shell" {
  #   script = "../scripts/install_chocolatey.bat"
  # }

  # # https://github.com/StefanScherer/packer-windows/blob/main/scripts/openssh.ps1
  # provisioner "powershell" {
  #   script = "../scripts/install_openssh.ps1"
  # }

  post-processor "vagrant" {
    keep_input_artifact = true
  }
}