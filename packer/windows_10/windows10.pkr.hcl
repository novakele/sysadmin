# Error: Could not determine network mappings from files in path
# Solution: https://www.vgemba.net/vmware/Packer-Workstation-Error/
packer {
  required_plugins {
    vmware = {
      version = ">= 1.0.3"
      source  = "github.com/hashicorp/vmware"
    }
  }
}

source "vmware-iso" "windows-10" {
  iso_url           = "https://software-download.microsoft.com/download/sg/444969d5-f34g-4e03-ac9d-1f9786c69161/19044.1288.211006-0501.21h2_release_svc_refresh_CLIENTENTERPRISEEVAL_OEMRET_x64FRE_en-us.iso"
  iso_checksum      = "sha256:69efac1df9ec8066341d8c9b62297ddece0e6b805533fdb6dd66bc8034fba27a"
  communicator      = "ssh"
  guest_os_type     = "windows9-64"
  ssh_username      = "vagrant"
  ssh_password      = "vagrant"
  ssh_timeout       = "30m"
  shutdown_command  = "cmd.exe /c shutdown /s /t 0 /f"
  cpus              = 4
  memory            = 4096 # 4GB
  cores             = 2
  disk_size         = 80000 # 80GB
  disk_adapter_type = "lsisas1068"
  sound             = false
  # https://www.windowscentral.com/how-create-unattended-media-do-automated-installation-windows-10#uefi_partition_setup
  floppy_files = [
    "./answers/Autounattend.xml",
    # https://github.com/StefanScherer/packer-windows/blob/main/scripts/enable-winrm.ps1
    "../scripts/enable_winrm.ps1",
    # https://github.com/StefanScherer/packer-windows/blob/main/scripts/openssh.ps1
    "../scripts/install_openssh.ps1",
    # https://github.com/StefanScherer/packer-windows/blob/main/scripts/chocolatey.bat
    "../scripts/install_chocolatey.bat",
  ]
  floppy_label = "floppy"
}

build {
  name = "windows 10"
  sources = [
    "source.vmware-iso.windows-10"
  ]

  post-processor "vagrant" {
    keep_input_artifact = false
  }
}