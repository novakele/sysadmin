terraform {
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "0.6.2"
      # https://github.com/dmacvicar/terraform-provider-libvirt/blob/master/docs/migration-13.md
    }
  }
}

provider "libvirt" {
    uri = "qemu:///system"
}

variable "domain_name" {
    description = "Domain used for DNS resolution"
}

resource "libvirt_network" "inside" {
    name = "inside"
    mode = "route"
    addresses = ["10.3.0.0/24"]
    domain = var.domain_name
    dns {
        enabled = true
        local_only = true   
    }
}

resource "libvirt_network" "management" {
    name = "management"
    mode = "route"
    addresses = ["10.1.0.0/24"]
    domain = var.domain_name
    dns {
        enabled = true
        local_only = true   
    }
}

resource "libvirt_network" "internet" {
    name = "internet"
    mode = "nat"
    domain = var.domain_name
    addresses = ["10.0.0.0/24"]
    dhcp {
        enabled = true   
    }
}
