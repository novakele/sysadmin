terraform {
  required_providers {
    digitalocean = {
      source = "terraform-providers/digitalocean"
    }
  }
  required_version = ">= 0.13"
}

variable "do_token" {
  description = "DigitalOcean API token"
}
variable "ssh_key_id" {
  description = "DigitalOcean SSH key ID"
}

provider "digitalocean" {
  token = var.do_token
}

locals {
  domain_name = "brokendomain.ca"
  region = "tor1"
  image = "centos-8-x64"
  size = "s-1vcpu-1gb"
  project_name = "BrokenDomain"
  vpc_name = "cluster-backend"
  vpc_network = "10.0.0.0/24"
}

data "digitalocean_domain" "domain" {
  name = local.domain_name
}

data "digitalocean_project" "project" {
  name = local.project_name
}

resource "digitalocean_vpc" "vpc" {
  name = local.vpc_name
  region = local.region
  ip_range = local.vpc_network
}

resource "digitalocean_droplet" "lb0" {
  image = local.image
  name = format("lb0.%s",local.domain_name)
  region = local.region
  size = local.size
  ipv6 = false
  ssh_keys = var.ssh_key_id
  private_networking = true
  vpc_uuid = digitalocean_vpc.vpc.id
}

resource "digitalocean_droplet" "lb1" {
  image = local.image
  name = format("lb1.%s",local.domain_name)
  region = local.region
  size = local.size
  ipv6 = false
  ssh_keys = var.ssh_key_id
  private_networking = true
  vpc_uuid = digitalocean_vpc.vpc.id
}

resource "digitalocean_floating_ip" "ip" {
  droplet_id = digitalocean_droplet.lb0.id
  region = digitalocean_droplet.lb0.region
}

resource "digitalocean_record" "lb0" {
  domain = local.domain_name
  name = split(".", digitalocean_droplet.lb0.name)[0]
  type = "A"
  ttl = 600
  value = digitalocean_droplet.lb0.ipv4_address
}

resource "digitalocean_record" "lb1" {
  domain = local.domain_name
  name = split(".", digitalocean_droplet.lb1.name)[0]
  type = "A"
  ttl = 600
  value = digitalocean_droplet.lb1.ipv4_address
}

resource "digitalocean_project_resources" "project" {
  project = data.digitalocean_project.project.id
  resources = [
    digitalocean_droplet.lb0.urn,
    digitalocean_droplet.lb1.urn,
    digitalocean_floating_ip.ip.urn,
    data.digitalocean_domain.domain.urn
  ]
}