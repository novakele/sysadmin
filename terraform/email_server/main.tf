terraform {
  required_providers {
    digitalocean = {
      source = "terraform-providers/digitalocean"
    }
  }
  required_version = ">= 0.13"
}

variable "do_token" {
  description = "DigitalOcean API token"
}
variable "ssh_key_id" {
  description = "DigitalOcean SSH key ID"
}

provider "digitalocean" {
  token = var.do_token
}

locals {
  image = "centos-8-x64"
  region = "tor1"
  size = "s-1vcpu-1gb"
  domain_name = "brokenserver.ca"
}

resource "digitalocean_droplet" "mail" {
  image = local.image
  name = format("mail.%s",local.domain_name)
  region = local.region
  size = local.size
  ipv6 = false
  ssh_keys = var.ssh_key_id
}

resource "digitalocean_floating_ip" "float_ip" {
  droplet_id = digitalocean_droplet.mail.id
  region = digitalocean_droplet.mail.region
}

resource "digitalocean_record" "a" {
  domain = local.domain_name
  type =  "A"
  name = split(".", digitalocean_droplet.mail.name)[0]
  value = digitalocean_droplet.mail.ipv4_address
}

resource "digitalocean_project" "project" {
  name = "EmailServers"
  description = "A project to test running my own email servers"
  purpose = "Send and Receive emails"
  environment = "development"
  resources = [
    digitalocean_droplet.mail.urn
  ]
}
